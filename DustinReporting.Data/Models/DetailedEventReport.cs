﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DustinReporting.Data.Models
{
    public class DetailedEventReport
    {
        public string SystemId { get; set; }
        public string Surname { get; set; }
        public string FirstNames { get; set; }
        public  DateTime Datetime { get; set; }
       // public string DateDisplay { get; set; }
        public TimeSpan Time { get; set; }
        public string EventDuration { get; set; }
        public string SensorStatus { get; set; }       
        public string Company { get; set; }
        public string ApparelType { get; set; }
        public string ProjectTeam { get; set; }
        public string EquipmentType { get; set; }
        public string OperatorName { get; set; }
        public string Position { get; set; }
        public string Supervisor { get; set; }
        public string ApparelUniqueId { get; set; }


        public string Diff { get; set; }
        //public string SensorId { get; set; }
        //public string SensorNodeId { get; set; }
        //public string DetectionLog { get; set; }
        //public string IsTimeValid { get; set; }
        //public string UniqueID { get; set; }


    }
}
