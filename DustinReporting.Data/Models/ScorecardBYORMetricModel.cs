﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DustinReporting.Data.Models
{
    public class ScorecardBYORMetricModel
    {
        public string SystemID { get; set; }
        public string Company { get; set; }
        public string ProjectTeam { get; set; }
        public string Name { get; set; }
        public string LastName { get; set; }
        public string FirstName { get; set; }        
        public DateTime Datetime { get; set; }
        public string SensorNodeId { get; set; }
        public string SensorStatus { get; set; }
        public string ApparelUniqueId { get; set; }
        public TimeSpan EventDuration { get; set; }
        public int NumberOfEvent { get; set; }
        public double ByorMetric { get; set; }
        public double FinalByorMetric { get; set; }


        //NumberOfEvent = 0   EventDuration = 0, 1 sec   ByorMetric = 0
        //NumberOfEvent = 1   EventDuration = 0, 1 sec   ByorMetric = 0.25 * EventDuration
        //NumberOfEvent = 2   EventDuration = 0, 1 sec   ByorMetric = 0.50 * EventDuration
        //NumberOfEvent = 3   EventDuration = 0, 1 sec   ByorMetric = 1 * EventDuration
        //NumberOfEvent = 4   EventDuration = 0, 1 sec   ByorMetric = 1 * EventDuration

    }

    public class ScorecardBYORMetricModelForExcel
    {
        public string SystemID { get; set; }
        public string Company { get; set; }
        public string ProjectTeam { get; set; }
        public string LastName { get; set; }
        public string FirstName { get; set; }
        public double ByorMetric { get; set; }
    }
}
