﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DustinReporting.Data.Models
{
    public class UserProfileModel
    {
        public Guid EmployeeId { get; set; }

        [DataType(DataType.EmailAddress)]
        [StringLength(128)]
        [Required()]
        public string EmailId { get; set; }

        [Display(Name = "Password")]
        [Required()]
        // [RegularExpression(@"^.*(?=.*[A-Z])(?=.*[0-9])(?=.*[!@#$%^&*\(\)_\-+=]).*$", ErrorMessage = "User_Password_Expression")]
        [StringLength(20, MinimumLength = 6, ErrorMessage = "length err")]
        [DataType(DataType.Password)]
        public string Password { get; set; }


      
        public string ProfilePicture { get; set; }

        public bool RememberMe { get; set; }
       

        [Required(ErrorMessage = "Please Enter First Name")]
        [Display(Name = "First Name")]
        public string FirstName { get; set; }

        [Required(ErrorMessage = "Please Enter Last Name")]
        [Display(Name = "Last Name")]
        public string LastName { get; set; }


       
    

        [Required(ErrorMessage = "Please Enter Phone Number")]
        [Display(Name = "Phone Number")]
        public string PhoneNo { get; set; }

        [Required(ErrorMessage = "Please Enter Company Name")]
        [Display(Name = "Company Name")]
        public string CompanyName { get; set; }

        [Required(ErrorMessage = "Please Enter Address")]
        [Display(Name = "Address")]
        public string Address { get; set; }

    }

    public class RegisterModel
    {
       public Guid EmployeeId { get; set; }
        public Int64 RoleId { get; set; }

        [Display(Name = "First Name")]
        public string FirstName { get; set; }

        [Display(Name = "Last Name")]
        public string LastName { get; set; }

        [DataType(DataType.EmailAddress)]
        [StringLength(128)]
        [Required()]
        public string EmailId { get; set; }

     
        //[RegularExpression(@"^.*(?=.*[A-Z])(?=.*[0-9])(?=.*[!@#$%^&*\(\)_\-+=]).*$", ErrorMessage = "User_Password_Expression")]
        [StringLength(20, MinimumLength = 6, ErrorMessage = "length err")]
        [DataType(DataType.Password)]
        public string Password { get; set; }

        public DateTime CreatedDate { get; set; }
        public bool IsActive { get; set; }

        public string Name { get; set; }
        public bool Status { get; set; }
        public string Info { get; set; }
        public string UserName { get; set; }
        public string Email { get; set; }
        public string EmpId { get; set; }
        public string ProfilePicture { get; set; }

        public List<RegisterModel> RegisterList { get; set; }
    }

    public class ForgetPasswordModel
    {
        [DataType(DataType.EmailAddress)]
        [StringLength(128)]
        [Required()]
        [Display(Name = "Email Id")]
        public string EmailId { get; set; }
    }

    public class ResetPasswordModel
    {
        [Display(Name = "New Password")]
        //[RegularExpression(@"^.*(?=.*[A-Z])(?=.*[0-9])(?=.*[!@#$%^&*\(\)_\-+=]).*$", ErrorMessage = "User_Password_Expression")]
        [StringLength(20, MinimumLength = 6, ErrorMessage = "length err")]
        [DataType(DataType.Password)]
        public string Password { get; set; }


        [Display(Name = "Confirm Password")]
        [Compare("Password", ErrorMessage = "Confirm password doesn't match, Type again !")]
        public string ConfirmPassword { get; set; }

    }

    public class UserDetails
    {
        public Guid EmployeeId { get; set; }
        public string NAME { get; set; }
        public string EmailId { get; set; }
        public bool status { get; set; }
        public string ProfilePicture { get; set; }
    }
    public class EditUser
    {
       
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public bool status { get; set; }
        public string ProfilePicture { get; set; }
    }

}
