﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DustinReporting.Data.Models
{
    public class SensorAssuranceModel
    {
        public string SensorNodeID { get; set; }
        public DateTime LastDownloadDate { get; set; }
        public string EquipmentType { get; set; }
        public string ProjectTeam { get; set; }
        public string Project { get; set; }
        public string Company { get; set; }
        public string Location { get; set; }
        public string OperatorName { get; set; }
    }
}
