﻿using DustinReporting.Admin.Helper;
using DustinReporting.Data;
using DustinReporting.Data.Models;
using iTextSharp.text;
using iTextSharp.text.html.simpleparser;
using iTextSharp.text.pdf;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace DustinReporting.Admin.Controllers
{
    public class ReportController : Controller
    {
        private WebReportingDbContext _objWebReportingDbContext = new WebReportingDbContext();

        public ReportController()
        {
            this._objWebReportingDbContext = new WebReportingDbContext();
        }
        
        /// <summary>
        /// Detailed Event Report
        /// </summary>
        /// <param name="company"></param>
        /// <param name="project"></param>
        /// <param name="datefrom"></param>
        /// <param name="dateto"></param>
        /// <returns></returns>
        [HttpGet]
        public ActionResult DetailedEventReport(string company, string project, DateTime? datefrom, DateTime? dateto)
        {
            // TimeSpan timedifd=new TimeSpan();      
            try
            {
                object values = null;
                if (this.TempData.TryGetValue("SearchConditions", out values))
                {
                    Common.searchConditions = values as Dictionary<string, object>;
                }
                Common.SetSearchConditionValue(Common.searchConditions, "company", company);
                Common.SetSearchConditionValue(Common.searchConditions, "project", project);
                Common.SetSearchConditionValue(Common.searchConditions, "datefrom", datefrom);
                Common.SetSearchConditionValue(Common.searchConditions, "dateto", dateto);

                TempData["SearchConditions"] = Common.searchConditions;

                List<DetailedEventReport> model = GetDetailedEventForExport(company,project, datefrom,dateto);  

                return View(model);
            }
            catch (Exception ex)
            {
                return Json(ex.Message);
            }
        }

        public List<DetailedEventReport> GetDetailedEventForExport(string company,string project, DateTime? datefrom, DateTime? dateto)
        {
            List<DetailedEventReport> model = new List<DetailedEventReport>();
            try
            {
                DateTime to = Convert.ToDateTime(dateto);

                DateTime from = Convert.ToDateTime(datefrom);


                object a = (object)to.ToString("yyyy-MM-dd");
                if (company!=null || project!=null || datefrom!=null || dateto!=null)
                {
                    if (datefrom != null && dateto != null)
                    {
                        model = _objWebReportingDbContext.Database.SqlQuery<DetailedEventReport>("ApparelVestsReport @Company=@Company,@Project=@Project,@DateFrom=@DateFrom,@DateTo=@DateTo",
                            new SqlParameter("Company", company),
                            new SqlParameter("Project", project),
                            new SqlParameter("DateFrom", from.ToString("yyyy-MM-dd")),
                            new SqlParameter("DateTo", to.ToString("yyyy-MM-dd"))).ToList();
                    }
                    else
                    {
                        model = _objWebReportingDbContext.Database.SqlQuery<DetailedEventReport>("ApparelVestsReport @Company=@Company,@Project=@Project",
                           new SqlParameter("Company", company),
                           new SqlParameter("Project", project)).ToList();
                    }

                    foreach (var item in model)
                    {
                        item.ApparelType = "Vest";
                    }

                    List<DetailedEventReport> list = new List<Data.Models.DetailedEventReport>();
                    if (datefrom != null && dateto != null)
                    {
                        list = _objWebReportingDbContext.Database.SqlQuery<DetailedEventReport>("ApparelHardHatReport @Company=@Company, @Project=@Project ,@DateFrom=@DateFrom, @DateTo=@DateTo",
                           new SqlParameter("Company", company),
                           new SqlParameter("Project", project),
                           new SqlParameter("DateFrom", from.ToString("yyyy-MM-dd")),
                           new SqlParameter("DateTo", to.ToString("yyyy-MM-dd"))).ToList();
                    }
                    else
                    {
                        list = _objWebReportingDbContext.Database.SqlQuery<DetailedEventReport>("ApparelHardHatReport @Company=@Company, @Project=@Project",
                           new SqlParameter("Company", company),
                           new SqlParameter("Project", project)                           
                           ).ToList();
                    }

                    foreach (var item in list)
                    {
                        item.ApparelType = "Hard Hat";
                        model.Add(item);
                    }

                    for (int i = 0; i < model.Count() - 1; i++)
                    {
                        TimeSpan timedifd = new TimeSpan(0);
                        if (model[i].ApparelUniqueId == model[i + 1].ApparelUniqueId && (model[i].SensorStatus == "1" && model[i + 1].SensorStatus == "2") && (model[i].Datetime<= model[i+1].Datetime))
                        {
                            timedifd = model[i + 1].Datetime - model[i].Datetime;
                            model[i].EventDuration = timedifd.ToString(); 
                        }
                        else
                        {
                            model[i].EventDuration = "";
                        }
                        switch ((model[i].SensorStatus))
                        {
                            case "0":
                                model[i].SensorStatus = "System info code";
                                break;
                            case "1":
                                model[i].SensorStatus = "Apparel entered field";
                                break;
                            case "2":
                                model[i].SensorStatus = "Apparel exited field";
                                break;
                            default:
                                break;
                        }
                    }

                }
                else
                {
                    model = _objWebReportingDbContext.Database.SqlQuery<DetailedEventReport>("ApparelVestsReport").ToList();

                    foreach (var item in model)
                    {
                        item.ApparelType = "Vest";
                    }

                    var list = _objWebReportingDbContext.Database.SqlQuery<DetailedEventReport>("ApparelHardHatReport").ToList();
                    foreach (var item in list)
                    {
                        item.ApparelType = "Hard Hat";
                        model.Add(item);
                    }
                  
                    for (int i = 0; i < model.Count() - 1; i++)
                    {
                        TimeSpan timedifd = new TimeSpan(0);
                        if (model[i].ApparelUniqueId == model[i + 1].ApparelUniqueId && (model[i].SensorStatus == "1" && model[i + 1].SensorStatus == "2") && (model[i].Datetime <= model[i + 1].Datetime))
                        {
                            timedifd = model[i + 1].Datetime - model[i].Datetime;
                            //model[i].Diff = timedifd.ToString();
                            model[i].EventDuration = timedifd.ToString();
                        }
                        else
                        {
                            model[i].EventDuration = "";
                        }

                        switch ((model[i].SensorStatus))
                        {
                            case "0":
                                model[i].SensorStatus = "System info code";
                                break;
                            case "1":
                                model[i].SensorStatus = "Apparel entered field";
                                break;
                            case "2":
                                model[i].SensorStatus = "Apparel exited field";
                                break;
                            default:
                                break;
                        }
                    }

                }

                //List <DetailedEventReport> s = model.Where(x => x.EventDuration != null || x.EventDuration != "").ToList();

                List<DetailedEventReport> b = (from i in model where i.EventDuration != null && i.EventDuration != "" select i).ToList();

                return b;
            }

            catch (Exception ex)
            {
                return null;
            }

        }

        public void ExportDetailedEventListToExcel()
        {
            object values = null;

            if (this.TempData.TryGetValue("SearchConditions", out values))
            {
                Common.searchConditions = values as Dictionary<string, object>;
            }

            object company = Common.GetSearchConditionValue(Common.searchConditions, "company");
            object project = Common.GetSearchConditionValue(Common.searchConditions, "project");
            object datefrom = Common.GetSearchConditionValue(Common.searchConditions, "datefrom");
            object dateto = Common.GetSearchConditionValue(Common.searchConditions, "dateto");
            
            var grid = new System.Web.UI.WebControls.GridView();
            List<DetailedEventReport> model = GetDetailedEventForExport(company != null ? company.ToString() : null, project != null ? project.ToString() : null, datefrom != null ? Convert.ToDateTime(datefrom) : (DateTime?)null, dateto != null ? Convert.ToDateTime(dateto) : (DateTime?)null);
          
            string headerTable = @"<Table border=1><tr></tr><tr>
                                    <td>System Id</td>
                                    <td colspan = 2 style=word-wrap: break-word, text-align: center;>Individual Last Name, First Name</td>
                                    <td>Date</td>
                                    <td>Time</td>
                                    <td>Event Duration</td>
                                  
                                    <td>Company</td>
                                    <td>Apparel Type</td>
                                    <td>Project Team</td>
                                    <td>EquipmentType</td>
                                    <td>OperatorName</td>
                                    <td>Position</td>
                                    <td>Supervisor</td>
                                    <td>ApparelUniqueId</td>
                                    </tr>
                                    <tr>
                                    <td>TCO Badge number if available, Company ID if not</td>
                                    <td colspan = 2 style=word-wrap: break-word, text-align: center;>Individual identified by Unique IDs from Apparel, Hard and/or Vest</td>
                                    <td>From Sensor Log Data</td>.................................................
                                    <td>From Sensor Log Data</td>
                                    <td>From Sensor Log Data - Time Unique ID enters and exits sensor zone</td>
                               
                                    <td>Sicim</td>
                                    <td>Apparel type identified by Unique IDs from Hard Hat and Vest</td>
                                    <td>CaTRo</td>
                                    <td>Equipment identified by Unique IDs from Sensor Node and Display Unit</td>
                                    <td>Operator assigned to machines, requires manual data entry or Excel Upload</td>
                                    <td>Position</td>
                                    <td>Supervisor</td>
                                    <td>ApparelUniqueId</td>
                                    </tr>
                                   </Table>";
            
            grid.ShowHeader = false;

            grid.DataSource = Mapper.MapDetailEventReport(model);
            grid.DataBind();

            Response.Clear();
            Response.Buffer = true;
            Response.AddHeader("content-disposition",
             "attachment;filename=DetailedEventExport.xls");
            Response.Charset = "";
            Response.ContentType = "application/vnd.ms-excel";
            StringWriter sw = new StringWriter();
            HtmlTextWriter hw = new HtmlTextWriter(sw);

            for (int i = 0; i < grid.Rows.Count; i++)
            {
                GridViewRow row = grid.Rows[i];
                row.Attributes.Add("class", "textmode");//Apply text style to each Row
                //row.Cells[i].Attributes.Add("style", "textmode");
            }
            
            grid.RenderControl(hw);

            //style to format numbers to string
            string style = @"<style> .textmode {   text-align: center; } </style>";
            Response.Write(style);

            Response.Write(headerTable);
            Response.Output.Write(sw.ToString());
            Response.Flush();
            Response.End();
        }
        
        public void ExportDetailedEventToPdf()
        {
            try
            {
                object values = null;

                if (this.TempData.TryGetValue("SearchConditions", out values))
                {
                    Common.searchConditions = values as Dictionary<string, object>;
                }

                object company = Common.GetSearchConditionValue(Common.searchConditions, "company");
                object project = Common.GetSearchConditionValue(Common.searchConditions, "project");
                object datefrom = Common.GetSearchConditionValue(Common.searchConditions, "datefrom");
                object dateto = Common.GetSearchConditionValue(Common.searchConditions, "dateto");
                
                var grid = new System.Web.UI.WebControls.GridView();
                List<DetailedEventReport> model = GetDetailedEventForExport(company != null ? company.ToString() : null, project != null ? project.ToString() : null, datefrom != null ? Convert.ToDateTime(datefrom) : (DateTime?)null, dateto != null ? Convert.ToDateTime(dateto) : (DateTime?)null);
                grid.DataSource = model;
                string date = DateTime.Now.ToString("d-M-yyyy");
                Response.ContentType = "application/pdf";
                Response.AddHeader("content-disposition", "attachment;filename=DetailedEventReport-" + date + ".pdf");
                Response.Cache.SetCacheability(HttpCacheability.NoCache);
                StringWriter sw = new StringWriter();
                HtmlTextWriter hw = new HtmlTextWriter(sw);
               // grid.AllowPaging = true;
                grid.DataBind();
                grid.RenderControl(hw);
                StringReader sr = new StringReader(sw.ToString());
                Document pdfDoc = new Document(PageSize.A4, 10f, 10f, 10f, 0f);
                HTMLWorker htmlparser = new HTMLWorker(pdfDoc);
                PdfWriter.GetInstance(pdfDoc, Response.OutputStream);
                pdfDoc.Open();
                htmlparser.Parse(sr);
                pdfDoc.Close();
                Response.Write(pdfDoc);
                Response.End();
            }
            catch(Exception ex)
            {

            }
          

        }
        
        /// <summary>
        /// Sensor Download Assurance
        /// </summary>
        /// <param name="company"></param>
        /// <param name="project"></param>
        /// <param name="datefrom"></param>
        /// <param name="dateto"></param>
        /// <returns></returns>
        public ActionResult SensorAssurance(string company, string project, DateTime? datefrom, DateTime? dateto)
        {
            //List<SensorAssuranceModel> model = new List<SensorAssuranceModel>();
            try
            {
                object values = null;
                if (this.TempData.TryGetValue("SearchConditions", out values))
                {
                    Common.searchConditions = values as Dictionary<string, object>;
                }
                Common.SetSearchConditionValue(Common.searchConditions, "company", company);
                Common.SetSearchConditionValue(Common.searchConditions, "project", project);
                Common.SetSearchConditionValue(Common.searchConditions, "datefrom", datefrom);
                Common.SetSearchConditionValue(Common.searchConditions, "dateto", dateto);

                TempData["SearchConditions"] = Common.searchConditions;

                List<SensorAssuranceModel> model = GetSensorAssurance(company, project, datefrom, dateto);

                return View(model);
            }
            catch (Exception ex)
            {
                return Json(ex.Message);
            }


        }

        public List<SensorAssuranceModel> GetSensorAssurance(string company, string project, DateTime? datefrom, DateTime? dateto)
        {
            List<SensorAssuranceModel> model = new List<SensorAssuranceModel>();
            try
            {
                DateTime to = Convert.ToDateTime(dateto);
                DateTime from = Convert.ToDateTime(datefrom);
                object a = (object)to.ToString("yyyy-MM-dd");
                if (company != null || project != null || datefrom != null || dateto != null)
                {
                    if (datefrom != null && dateto != null)
                    {
                        model = _objWebReportingDbContext.Database.SqlQuery<SensorAssuranceModel>("ListOfSensorAssuranceHardHat @Company=@Company,@Project=@Project,@DateFrom=@DateFrom,@DateTo=@DateTo",
                            new SqlParameter("Company", company),
                            new SqlParameter("Project", project),
                            new SqlParameter("DateFrom", from.ToString("yyyy-MM-dd")),
                            new SqlParameter("DateTo", to.ToString("yyyy-MM-dd"))).ToList();
                    }
                    else
                    {
                        model = _objWebReportingDbContext.Database.SqlQuery<SensorAssuranceModel>("ListOfSensorAssuranceHardHat @Company=@Company,@Project=@Project",
                           new SqlParameter("Company", company),
                           new SqlParameter("Project", project)).ToList();
                    }
                    List<SensorAssuranceModel> list = new List<Data.Models.SensorAssuranceModel>();
                    foreach (var item in list)
                    {
                        model.Add(item);

                    }
                  
                    if (datefrom != null && dateto != null)
                    {

                        list = _objWebReportingDbContext.Database.SqlQuery<SensorAssuranceModel>("ListOfSensorAssuranceVest @Company=@Company,@Project=@Project,@DateFrom=@DateFrom,@DateTo=@DateTo",
                           new SqlParameter("Company", company),
                           new SqlParameter("Project", project),
                           new SqlParameter("DateFrom", from.ToString("yyyy-MM-dd")),
                           new SqlParameter("DateTo", to.ToString("yyyy-MM-dd"))).ToList();
                    }
                    else
                    {

                        list = _objWebReportingDbContext.Database.SqlQuery<SensorAssuranceModel>("ListOfSensorAssuranceVest @Company=@Company,@Project=@Project",
                           new SqlParameter("Company", company),
                           new SqlParameter("Project", project)).ToList();
                    }
                }
                else
                {
                    model = _objWebReportingDbContext.Database.SqlQuery<SensorAssuranceModel>("ListOfSensorAssuranceHardHat").ToList();
                    var list = _objWebReportingDbContext.Database.SqlQuery<SensorAssuranceModel>("ListOfSensorAssuranceVest").ToList();

                    foreach (var item in list)
                    {
                        model.Add(item);

                    }

                }

                //var q9 = (from n in model
                //          group n by n.SensorNodeID).ToList();

                List<SensorAssuranceModel> q = (from n in model
                         group n by n.SensorNodeID into g
                         select new SensorAssuranceModel
                         {
                             SensorNodeID = g.Key,
                             LastDownloadDate = g.Max(t => t.LastDownloadDate),
                             Company = g.Where(x => x.SensorNodeID == g.Key && x.LastDownloadDate == g.Max(t => t.LastDownloadDate)).Select(x => x.Company).FirstOrDefault(),
                             EquipmentType = g.Where(x => x.SensorNodeID == g.Key && x.LastDownloadDate == g.Max(t => t.LastDownloadDate)).Select(x => x.EquipmentType).FirstOrDefault(),
                             ProjectTeam = g.Where(x => x.SensorNodeID == g.Key && x.LastDownloadDate == g.Max(t => t.LastDownloadDate)).Select(x => x.ProjectTeam).FirstOrDefault(),
                             Project = g.Where(x => x.SensorNodeID == g.Key && x.LastDownloadDate == g.Max(t => t.LastDownloadDate)).Select(x => x.Project).FirstOrDefault(),
                             Location = g.Where(x => x.SensorNodeID == g.Key && x.LastDownloadDate == g.Max(t => t.LastDownloadDate)).Select(x => x.Location).FirstOrDefault(),
                             OperatorName = g.Where(x => x.SensorNodeID == g.Key && x.LastDownloadDate == g.Max(t => t.LastDownloadDate)).Select(x => x.OperatorName).FirstOrDefault()
                         }).ToList();


                return q;
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public void ExportSensorAssuranceListToExcel()
        {
            object values = null;

            if (this.TempData.TryGetValue("SearchConditions", out values))
            {
                Common.searchConditions = values as Dictionary<string, object>;
            }

            object company = Common.GetSearchConditionValue(Common.searchConditions, "company");
            object project = Common.GetSearchConditionValue(Common.searchConditions, "project");
            object datefrom = Common.GetSearchConditionValue(Common.searchConditions, "datefrom");
            object dateto = Common.GetSearchConditionValue(Common.searchConditions, "dateto");           

            var grid = new System.Web.UI.WebControls.GridView();
            List<SensorAssuranceModel> model = GetSensorAssurance(company != null ? company.ToString() : null, project != null ? project.ToString() : null, datefrom != null ? Convert.ToDateTime(datefrom) : (DateTime?)null, dateto != null ? Convert.ToDateTime(dateto) : (DateTime?)null);

            string headerTable = @"<Table border=1><tr></tr><tr>
                                    <td>Sensor ID</td>
                                    <td>Last Download</td>
                                    <td>Equipment Type</td>
                                    <td>Project Team</td>
                                    <td>Project Team - Second Level</td>
                                    <td>Company</td>
                                    <td>Location</td>
                                    <td>Operator</td>
                                    </tr>
                                    <tr>
                                    <td>List to identify sensor IDs that were missed in data extraction, and have not had data pulled and uploaded; list in order of duration since last download / upload</td>
                                    <td ></td>
                                    <td>Equipment identified by Unique IDs from Sensor Node and Display UnitSensor and Display Unit installed in machines, requires manual data entry or Excel Upload</td>
                                    <td style=text-align: center;>CaTRo</td>
                                    <td style=text-align: center;>CaTRo Construction</td>
                                    <td style=text-align: center;>Sicim</td>
                                    <td>Is it possible to determine location? If not, how can that be determined easily and inexpensively?</td>
                                    <td>Operator assigned to machines, requires manual data entry or Excel Upload</td>
                                    </tr>
                                   </Table>";

            grid.ShowHeader = false;
            grid.DataSource = model;
            grid.DataBind();

            

            Response.Clear();
            Response.Buffer = true;
            Response.AddHeader("content-disposition",
             "attachment;filename=SensorDownloadAssurance.xls");
            Response.Charset = "";
            Response.ContentType = "application/vnd.ms-excel";
            StringWriter sw = new StringWriter();
            HtmlTextWriter hw = new HtmlTextWriter(sw);

            for (int i = 0; i < grid.Rows.Count; i++)
            {
                GridViewRow row = grid.Rows[i];
                row.Attributes.Add("class", "textmode"); //Apply text style to each Row

            }
            grid.RenderControl(hw);
            string style = @"<style> .textmode {   text-align: center; } </style>";
            Response.Write(style);//style to format numbers to string

            Response.Write(headerTable);
            Response.Output.Write(sw.ToString());
            Response.Flush();
            Response.End();

        }

        public void ExportSensorAssuranceListToPdf()
        {
            object values = null;

            if (this.TempData.TryGetValue("SearchConditions", out values))
            {
                Common.searchConditions = values as Dictionary<string, object>;
            }

            object company = Common.GetSearchConditionValue(Common.searchConditions, "company");
            object project = Common.GetSearchConditionValue(Common.searchConditions, "project");
            object datefrom = Common.GetSearchConditionValue(Common.searchConditions, "datefrom");
            object dateto = Common.GetSearchConditionValue(Common.searchConditions, "dateto");

         
            var grid = new System.Web.UI.WebControls.GridView();
            List<SensorAssuranceModel> model = GetSensorAssurance(company != null ? company.ToString() : null, project != null ? project.ToString() : null, datefrom != null ? Convert.ToDateTime(datefrom) : (DateTime?)null, dateto != null ? Convert.ToDateTime(dateto) : (DateTime?)null);
            grid.DataSource =model;
            string date = DateTime.Now.ToString("d-M-yyyy");
            Response.ContentType = "application/pdf";
            Response.AddHeader("content-disposition", "attachment;filename=SensorAssuranceListReport-" + date + ".pdf");
            Response.Cache.SetCacheability(HttpCacheability.NoCache);
            StringWriter sw = new StringWriter();
            HtmlTextWriter hw = new HtmlTextWriter(sw);
            grid.AllowPaging = false;
            grid.DataBind();
            grid.RenderControl(hw);
            StringReader sr = new StringReader(sw.ToString());
            Document pdfDoc = new Document(PageSize.A4, 10f, 10f, 10f, 0f);
            HTMLWorker htmlparser = new HTMLWorker(pdfDoc);
            PdfWriter.GetInstance(pdfDoc, Response.OutputStream);
            pdfDoc.Open();
            htmlparser.Parse(sr);
            pdfDoc.Close();
            Response.Write(pdfDoc);
            Response.End();
        }
        
        /// <summary>
        /// Scorecard BYORMetric Report
        /// </summary>
        /// <param name="company"></param>
        /// <param name="project"></param>
        /// <param name="datefrom"></param>
        /// <param name="dateto"></param>
        /// <returns></returns>
        public ActionResult ScorecardBYORMetric(string company, string project, DateTime? datefrom, DateTime? dateto)
        {
            try
            {
                List<ScorecardBYORMetricModel> objList = GetScorecardBYORMetricList(company, project, datefrom, dateto);
                
                object values = null;
                if (this.TempData.TryGetValue("SearchConditions", out values))
                {
                    Common.searchConditions = values as Dictionary<string, object>;
                }
                Common.SetSearchConditionValue(Common.searchConditions, "company", company);
                Common.SetSearchConditionValue(Common.searchConditions, "project", project);
                Common.SetSearchConditionValue(Common.searchConditions, "datefrom", datefrom);
                Common.SetSearchConditionValue(Common.searchConditions, "dateto", dateto);

                TempData["SearchConditions"] = Common.searchConditions;


                return View(objList);
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public List<ScorecardBYORMetricModel> GetScorecardBYORMetricList(string company, string project, DateTime? datefrom, DateTime? dateto)
        {
            List<ScorecardBYORMetricModel> model = new List<ScorecardBYORMetricModel>();

            List<ScorecardBYORMetricModel> result = null;
            try
            {
                if (datefrom != null && dateto != null)
                {
                    DateTime to = Convert.ToDateTime(dateto);
                    DateTime from = Convert.ToDateTime(datefrom);
                    object a = (object)to.ToString("yyyy-MM-dd");

                    if (company != null || project != null || datefrom != null || dateto != null)
                    {
                        #region Get data from db

                        if (datefrom != null && dateto != null)
                        {
                            model = _objWebReportingDbContext.Database.SqlQuery<ScorecardBYORMetricModel>("ScorecardByorMetricHardHat @Company=@Company,@Project=@Project,@DateFrom=@DateFrom,@DateTo=@DateTo",
                                new SqlParameter("Company", company),
                                new SqlParameter("Project", project),
                                new SqlParameter("DateFrom", from.ToString("yyyy-MM-dd")),
                                new SqlParameter("DateTo", to.ToString("yyyy-MM-dd"))).ToList();
                        }
                        else
                        {
                            model = _objWebReportingDbContext.Database.SqlQuery<ScorecardBYORMetricModel>("ScorecardByorMetricHardHat @Company=@Company,@Project=@Project",
                              new SqlParameter("Company", company),
                              new SqlParameter("Project", project)).ToList();
                        }

                        if (datefrom != null && dateto != null)
                        {
                            model.AddRange(_objWebReportingDbContext.Database.SqlQuery<ScorecardBYORMetricModel>("ScorecardByorMetricVest @Company=@Company,@Project=@Project,@DateFrom=@DateFrom,@DateTo=@DateTo",
                               new SqlParameter("Company", company),
                               new SqlParameter("Project", project),
                               new SqlParameter("DateFrom", from.ToString("yyyy-MM-dd")),
                               new SqlParameter("DateTo", to.ToString("yyyy-MM-dd"))).ToList());
                        }
                        else
                        {
                            model.AddRange(_objWebReportingDbContext.Database.SqlQuery<ScorecardBYORMetricModel>("ScorecardByorMetricVest @Company=@Company,@Project=@Project",
                               new SqlParameter("Company", company),
                               new SqlParameter("Project", project)).ToList());
                        }
                    }
                    else
                    {
                        model = _objWebReportingDbContext.Database.SqlQuery<ScorecardBYORMetricModel>("ScorecardByorMetricHardHat").ToList();
                        model.AddRange(_objWebReportingDbContext.Database.SqlQuery<ScorecardBYORMetricModel>("ScorecardByorMetricVest").ToList());
                    }

                    #endregion

                    for (int i = 0; i < model.Count() - 1; i++)
                    {
                        TimeSpan timedifd = new TimeSpan(0);
                        if (model[i].ApparelUniqueId == model[i + 1].ApparelUniqueId && (model[i].SensorStatus == "1" && model[i + 1].SensorStatus == "2") && (model[i].Datetime <= model[i + 1].Datetime))
                        {
                            timedifd = model[i + 1].Datetime - model[i].Datetime;
                            model[i + 1].EventDuration = timedifd;
                        }
                    }

                    model = model.Where(x => x.SystemID != "").ToList();
                    model.OrderBy(x => x.SystemID).ToList();


                    string SystemId = "";
                    foreach (var item in model)
                    {
                        if (item.EventDuration != TimeSpan.Zero)
                        {
                            if (SystemId == item.SystemID)
                            {
                                int Number = model.Where(x => x.SystemID == SystemId && x.NumberOfEvent != 0).Select(x => x.NumberOfEvent).Last();
                                item.NumberOfEvent = Number + 1;
                            }
                            else
                            {
                                item.NumberOfEvent = 1;
                            }
                            SystemId = item.SystemID;
                        }
                    }

                    foreach (var item in model)
                    {
                        switch (item.NumberOfEvent)
                        {
                            case 0:
                                item.ByorMetric = 0;
                                break;

                            case 1:
                                item.ByorMetric = 0.25 * SecondsConverter.Convert(item.EventDuration);
                                break;

                            case 2:
                                item.ByorMetric = 0.50 * SecondsConverter.Convert(item.EventDuration);
                                break;

                            case 3:
                                item.ByorMetric = 1 * SecondsConverter.Convert(item.EventDuration);
                                break;

                            default:
                                item.ByorMetric = 1 * SecondsConverter.Convert(item.EventDuration);
                                break;
                        }
                    }

                    result = (from n in model
                                        group n by n.SystemID into g
                                        select new ScorecardBYORMetricModel
                                        {
                                            SystemID = g.Key,
                                            ByorMetric = g.Where(x => x.SystemID == g.Key).Sum(x => x.ByorMetric),
                                            Company = g.Where(x => x.SystemID == g.Key).Select(x => x.Company).FirstOrDefault(),
                                            ProjectTeam = g.Where(x => x.SystemID == g.Key).Select(x => x.ProjectTeam).FirstOrDefault(),
                                            FirstName = g.Where(x => x.SystemID == g.Key).Select(x => x.FirstName).FirstOrDefault(),
                                            LastName = g.Where(x => x.SystemID == g.Key).Select(x => x.LastName).FirstOrDefault()
                                        }).OrderBy(x => x.SystemID).ToList();

                    return result.ToList();
                }
                else
                {
                  
                    return new List<ScorecardBYORMetricModel>();
                }
            }
            catch (Exception ex)
            {
                return new List<ScorecardBYORMetricModel>();
            }
        }

        public void ExportBYORMetricListToExcel()
        {
            object values = null;

            if (this.TempData.TryGetValue("SearchConditions", out values))
            {
                Common.searchConditions = values as Dictionary<string, object>;
            }

            object company = Common.GetSearchConditionValue(Common.searchConditions, "company");
            object project = Common.GetSearchConditionValue(Common.searchConditions, "project");
            object datefrom = Common.GetSearchConditionValue(Common.searchConditions, "datefrom");
            object dateto = Common.GetSearchConditionValue(Common.searchConditions, "dateto");
            

            List<ScorecardBYORMetricModelForExcel> exportList = new List<ScorecardBYORMetricModelForExcel>();
            var grid = new System.Web.UI.WebControls.GridView();
            List<ScorecardBYORMetricModel> model = GetScorecardBYORMetricList(company != null ? company.ToString() : null, project != null ? project.ToString() : null, datefrom != null ? Convert.ToDateTime(datefrom) : (DateTime?)null, dateto != null ? Convert.ToDateTime(dateto) : (DateTime?)null);

            foreach (ScorecardBYORMetricModel item in model)
            {
                ScorecardBYORMetricModelForExcel obj = Mapper.MapByorMetricModel(item);
                exportList.Add(obj);
            }

            string headerTable = @"<Table border=1><tr></tr><tr>
                                    <td>System ID</td>
                                    <td></td>
                                    <td></td>
                                    <td colspan=2>Individual Last Name, First Name</td>
                                   
                                    <td>SCORECARD - BYOR METRIC</td>
                                    </tr>
                                    <tr>
                                    <td>TCO Badge number if available, Company ID if not</td>
                                    <td >Company</td>
                                    <td>Project</td>
                                    <td colspan=2>Individual identified by Unique IDs from Apparel, Hard and/or Vest</td>                                  
                                    <td>BYOR Metric</td>
                                    </tr>
                                   </Table>";
            
            grid.ShowHeader = false;
            grid.DataSource = exportList;
            grid.DataBind();



            Response.Clear();
            Response.Buffer = true;
            Response.AddHeader("content-disposition",
             "attachment;filename=SensorBYORMetric.xls");
            Response.Charset = "";
            Response.ContentType = "application/vnd.ms-excel";
            StringWriter sw = new StringWriter();
            HtmlTextWriter hw = new HtmlTextWriter(sw);

            for (int i = 0; i < grid.Rows.Count; i++)
            {
                GridViewRow row = grid.Rows[i];
                //Apply text style to each Row
                row.Attributes.Add("class", "textmode");
            }

            grid.RenderControl(hw);

            //style to format numbers to string
            string style = @"<style> .textmode {   text-align: center; } </style>";
            Response.Write(style);

            Response.Write(headerTable);
            Response.Output.Write(sw.ToString());
            Response.Flush();
            Response.End();

        }

        public void ExportBYORMetricListToPdf()
        {
            object values = null;

            if (this.TempData.TryGetValue("SearchConditions", out values))
            {
                Common.searchConditions = values as Dictionary<string, object>;
            }

            object company = Common.GetSearchConditionValue(Common.searchConditions, "company");
            object project = Common.GetSearchConditionValue(Common.searchConditions, "project");
            object datefrom = Common.GetSearchConditionValue(Common.searchConditions, "datefrom");
            object dateto = Common.GetSearchConditionValue(Common.searchConditions, "dateto");
                     

            List<ScorecardBYORMetricModelForExcel> exportList = new List<ScorecardBYORMetricModelForExcel>();
            var grid = new System.Web.UI.WebControls.GridView();
            List<ScorecardBYORMetricModel> model = GetScorecardBYORMetricList(company != null ? company.ToString() : null, project != null ? project.ToString() : null, datefrom != null ? Convert.ToDateTime(datefrom) : (DateTime?)null, dateto != null ? Convert.ToDateTime(dateto) : (DateTime?)null);

            foreach (ScorecardBYORMetricModel item in model)
            {
                ScorecardBYORMetricModelForExcel obj = Mapper.MapByorMetricModel(item);
                exportList.Add(obj);
            }
            grid.DataSource = exportList;
            string date = DateTime.Now.ToString("d-M-yyyy");
            Response.ContentType = "application/pdf";
            Response.AddHeader("content-disposition", "attachment;filename=SensorAssuranceListReport-" + date + ".pdf");
            Response.Cache.SetCacheability(HttpCacheability.NoCache);
            StringWriter sw = new StringWriter();
            HtmlTextWriter hw = new HtmlTextWriter(sw);
            grid.AllowPaging = false;
            grid.DataBind();
            grid.RenderControl(hw);
            StringReader sr = new StringReader(sw.ToString());
            Document pdfDoc = new Document(PageSize.A4, 10f, 10f, 10f, 0f);
            HTMLWorker htmlparser = new HTMLWorker(pdfDoc);
            PdfWriter.GetInstance(pdfDoc, Response.OutputStream);
            pdfDoc.Open();
            htmlparser.Parse(sr);
            pdfDoc.Close();
            Response.Write(pdfDoc);
            Response.End();

        }
        
        /// <summary>
        /// Sensor Assurance ScoreCard 
        /// </summary>
        /// <param name="company"></param>
        /// <param name="project"></param>
        /// <param name="datefrom"></param>
        /// <param name="dateto"></param>
        /// <returns></returns>
        public ActionResult SensorScoreCard(string company, string project, DateTime? datefrom, DateTime? dateto)
        {
            try
            {
                object values = null;
                if (this.TempData.TryGetValue("SearchConditions", out values))
                {
                    Common.searchConditions = values as Dictionary<string, object>;
                }
                Common.SetSearchConditionValue(Common.searchConditions, "company", company);
                Common.SetSearchConditionValue(Common.searchConditions, "project", project);
                Common.SetSearchConditionValue(Common.searchConditions, "datefrom", datefrom);
                Common.SetSearchConditionValue(Common.searchConditions, "dateto", dateto);

                TempData["SearchConditions"] = Common.searchConditions;

                List<SensorScoreCardModel> model = GetSensorScoreCardList(company, project, datefrom, dateto);

                return View(model);
            }
            catch (Exception ex)
            {
                return Json(ex.Message);
            }
        }

        public List<SensorScoreCardModel> GetSensorScoreCardList(string company, string project, DateTime? datefrom, DateTime? dateto)
        {
            List<SensorScoreCardModel> model = new List<SensorScoreCardModel>();
            try
            {
                DateTime to = Convert.ToDateTime(dateto);
                DateTime from = Convert.ToDateTime(datefrom);
                object a = (object)to.ToString("yyyy-MM-dd");
                if (company != null || project != null || datefrom != null || dateto != null)
                {
                    if (datefrom != null && dateto != null)
                    {
                        model = _objWebReportingDbContext.Database.SqlQuery<SensorScoreCardModel>("SensorScoreCardForHardHat @Company=@Company,@Project=@Project,@DateFrom=@DateFrom,@DateTo=@DateTo",
                           new SqlParameter("Company", company),
                           new SqlParameter("Project", project),
                           new SqlParameter("DateFrom", from.ToString("yyyy-MM-dd")),
                           new SqlParameter("DateTo", to.ToString("yyyy-MM-dd"))).ToList();
                    }
                    else
                    {
                        model = _objWebReportingDbContext.Database.SqlQuery<SensorScoreCardModel>("SensorScoreCardForHardHat @Company=@Company,@Project=@Project",
                          new SqlParameter("Company", company),
                          new SqlParameter("Project", project)).ToList();
                    }

                    List<SensorScoreCardModel> list = new List<Data.Models.SensorScoreCardModel>();
                    foreach (var item in list)
                    {
                        model.Add(item);
                    }

                    if (datefrom != null && dateto != null)
                    {
                        list = _objWebReportingDbContext.Database.SqlQuery<SensorScoreCardModel>("SensorScoreCardForVest @Company=@Company,@Project=@Project,@DateFrom=@DateFrom,@DateTo=@DateTo",
                          new SqlParameter("Company", company),
                          new SqlParameter("Project", project),
                          new SqlParameter("DateFrom", from.ToString("yyyy-MM-dd")),
                          new SqlParameter("DateTo", to.ToString("yyyy-MM-dd"))).ToList();

                    }
                    else
                    {
                        list = _objWebReportingDbContext.Database.SqlQuery<SensorScoreCardModel>("SensorScoreCardForVest @Company=@Company,@Project=@Project",
                         new SqlParameter("Company", company),
                         new SqlParameter("Project", project)).ToList();
                    }
                }
                else
                {
                    model = _objWebReportingDbContext.Database.SqlQuery<SensorScoreCardModel>("SensorScoreCardForHardHat").ToList();
                    var list = _objWebReportingDbContext.Database.SqlQuery<SensorScoreCardModel>("SensorScoreCardForVest").ToList();

                    foreach (var item in list)
                    {
                        model.Add(item);
                    }
                }           
                
            
                           
                return model;
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public void ExportSensorScoreCardListToExcel()
        {
            object values = null;

            if (this.TempData.TryGetValue("SearchConditions", out values))
            {
                Common.searchConditions = values as Dictionary<string, object>;
            }

            object company = Common.GetSearchConditionValue(Common.searchConditions, "company");
            object project = Common.GetSearchConditionValue(Common.searchConditions, "project");
            object datefrom = Common.GetSearchConditionValue(Common.searchConditions, "datefrom");
            object dateto = Common.GetSearchConditionValue(Common.searchConditions, "dateto");            

            var grid = new System.Web.UI.WebControls.GridView();
            List<SensorScoreCardModel> model = GetSensorScoreCardList(company != null ? company.ToString() : null, project != null ? project.ToString() : null, datefrom != null ? Convert.ToDateTime(datefrom) : (DateTime?)null, dateto != null ? Convert.ToDateTime(dateto) : (DateTime?)null);

            string headerTable = @"<Table border=1><tr></tr><tr>
                                    <td>Sensor ID</td>
                                    <td>Last Download</td>
                                    <td>Equipment Type</td>
                                    <td>Project Team</td>
                                    <td>Company</td>
                                    </tr>
                                    <tr>
                                    <td>List to identify sensor IDs that were missed in data extraction, and have not had data pulled and uploaded; list in order of duration since last download / upload</td>
                                    <td ></td>
                                    <td>Equipment identified by Unique IDs from Sensor Node and Display UnitSensor and Display Unit installed in machines, requires manual data entry or Excel Upload</td>
                                    <td>CaTRo</td>
                                    <td>Sicim</td>
                                    </tr>
                                   </Table>";

            grid.ShowHeader = false;
            grid.DataSource = model;
            grid.DataBind();

            Response.Clear();
            Response.Buffer = true;
            Response.AddHeader("content-disposition",
             "attachment;filename=SensorDownloadAssurance.xls");
            Response.Charset = "";
            Response.ContentType = "application/vnd.ms-excel";
            StringWriter sw = new StringWriter();
            HtmlTextWriter hw = new HtmlTextWriter(sw);

            for (int i = 0; i < grid.Rows.Count; i++)
            {
                GridViewRow row = grid.Rows[i];
                row.Attributes.Add("class", "textmode"); //Apply text style to each Row

            }
            grid.RenderControl(hw);
            string style = @"<style> .textmode {   text-align: center; } </style>";
            Response.Write(style);//style to format numbers to string

            Response.Write(headerTable);
            Response.Output.Write(sw.ToString());
            Response.Flush();
            Response.End();

        }

        public void ExportSensorScoreCardListToPdf()
        {
            object values = null;

            if (this.TempData.TryGetValue("SearchConditions", out values))
            {
                Common.searchConditions = values as Dictionary<string, object>;
            }

            object company = Common.GetSearchConditionValue(Common.searchConditions, "company");
            object project = Common.GetSearchConditionValue(Common.searchConditions, "project");
            object datefrom = Common.GetSearchConditionValue(Common.searchConditions, "datefrom");
            object dateto = Common.GetSearchConditionValue(Common.searchConditions, "dateto");
                    
            var grid = new System.Web.UI.WebControls.GridView();
            List<SensorScoreCardModel> model = GetSensorScoreCardList(company != null ? company.ToString() : null, project != null ? project.ToString() : null, datefrom != null ? Convert.ToDateTime(datefrom) : (DateTime?)null, dateto != null ? Convert.ToDateTime(dateto) : (DateTime?)null);
            grid.DataSource = model;
            string date = DateTime.Now.ToString("d-M-yyyy");
            Response.ContentType = "application/pdf";
            Response.AddHeader("content-disposition", "attachment;filename=SensorAssuranceListReport-" + date + ".pdf");
            Response.Cache.SetCacheability(HttpCacheability.NoCache);
            StringWriter sw = new StringWriter();
            HtmlTextWriter hw = new HtmlTextWriter(sw);
            grid.AllowPaging = false;
            grid.DataBind();
            grid.RenderControl(hw);
            StringReader sr = new StringReader(sw.ToString());
            Document pdfDoc = new Document(PageSize.A4, 10f, 10f, 10f, 0f);
            HTMLWorker htmlparser = new HTMLWorker(pdfDoc);
            PdfWriter.GetInstance(pdfDoc, Response.OutputStream);
            pdfDoc.Open();
            htmlparser.Parse(sr);
            pdfDoc.Close();
            Response.Write(pdfDoc);
            Response.End();

        }
    }
}
