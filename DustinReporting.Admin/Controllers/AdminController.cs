﻿using DustinReporting.Admin.Models;
using DustinReporting.Data;
using DustinReporting.Data.Models;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Configuration;
using System.Web.Mvc;

namespace DustinReporting.Admin.Controllers
{
    public class AdminController : Controller
    {
        private WebReportingDbContext _objWebReportingDbContext = new WebReportingDbContext();
        private string _profileImagesPath = WebConfigurationManager.AppSettings["ProfileImages"];


        [HttpGet]
        public ActionResult ProfileDetails()
        {
            try
            {
                UserProfileModel model = new UserProfileModel();
                //ApplicationSession.Login(model);
                //Session["Id"] = model.EmployeeId;

               var value= ApplicationSession.EmployeeId;

                model = _objWebReportingDbContext.Database.SqlQuery<UserProfileModel>("EditAdminDetails @EmployeeId=@EmployeeId",
                        new SqlParameter("EmployeeId", value)).FirstOrDefault();
                return View(model);
            }
            catch(Exception ex)
            {

            }

            return View();
        }



        [HttpPost]
        public ActionResult ProfileDetails(UserProfileModel model, HttpPostedFileBase file)
        {
            try
            {
                if (file != null)
                {
                    var allowedExtensions = new[] {
            ".Jpg", ".png", ".jpg", "jpeg"
             };
                    var fileName = Path.GetFileName(file.FileName); //getting only file name(ex-ganesh.jpg)  
                    var ext = Path.GetExtension(file.FileName); //getting the extension(ex-.jpg)  
                    if (allowedExtensions.Contains(ext)) //check what type of extension  
                    {
                        string name = Path.GetFileNameWithoutExtension(fileName); //getting file name without extension  
                        string myfile = name + "_" + model.FirstName + ext; //appending the name with id  
                                                                            // store the file inside ~/project folder(Img)  
                        var path = Path.Combine(Server.MapPath("~/UserProfilePic"), myfile);
                        model.ProfilePicture = _profileImagesPath + myfile;
                        // model.CreatedDate = ApplicationSession.CurrentUser.EmployeeId;
                        file.SaveAs(path);
                    }
                    else
                    {
                        ModelState.AddModelError("", "Please selet the iamge in .Jpg .png .jpg .jpeg Extensions Only!!");
                    }
                }

                    var value = ApplicationSession.EmployeeId;

                int roweffected = _objWebReportingDbContext.Database.ExecuteSqlCommand("UpdateAdminDetails @EmployeeId=@EmployeeId,@FirstName=@FirstName,@LastName=@LastName,@EmailId=@EmailId,@ProfilePicture=@ProfilePicture,@PhoneNo=@PhoneNo,@CompanyName=@CompanyName,@Address=@Address",
                        new SqlParameter("EmployeeId", value),
                         new SqlParameter("FirstName", model.FirstName),
                          new SqlParameter("LastName", model.LastName),
                           new SqlParameter("EmailId", model.EmailId),
                           
                          new SqlParameter("ProfilePicture", (object)model.ProfilePicture ?? DBNull.Value),
                          new SqlParameter("PhoneNo", model.PhoneNo),
                          new SqlParameter("CompanyName", model.CompanyName),
                          new SqlParameter("Address", model.Address));
                

                if(roweffected>0)
                {

                    TempData["message"] = "Your profile Is updated Successfully";
                    return RedirectToAction("DashBoard", "UserProfile");
                }
                else
                {
                    TempData["message"] = "Please Re-enter the details";
                    return RedirectToAction("ProfileDetails");
                }
            }
            catch (Exception ex)
            {
                return Json(ex.Message);
            }

            return View();
        }
    }
}
