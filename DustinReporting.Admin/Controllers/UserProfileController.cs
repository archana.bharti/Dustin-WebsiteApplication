﻿using DustinReporting.Admin.Models;
using DustinReporting.Data;
using DustinReporting.Data.Models;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Threading.Tasks;
using System.Web;
using System.Web.Configuration;
using System.Web.Mvc;
using System.Web.Security;

namespace DustinReporting.Admin.Controllers
{
    public class UserProfileController : Controller
    {
        private WebReportingDbContext _objWebReportingDbContext = new WebReportingDbContext();
        //string ChangePasswordUrl = WebConfigurationManager.AppSettings["DWRLocal"];
        string ChangePasswordUrl = WebConfigurationManager.AppSettings["DWROnline"];
        HttpCookie cookie = new HttpCookie("Login");
        private string _profileImagesPath = WebConfigurationManager.AppSettings["ProfileImages"];

        [HttpGet]
        public ActionResult Login()
        {

            UserProfileModel model = new UserProfileModel();
            if (Request.Cookies["Login"] != null)
            {
                model.EmailId = Request.Cookies["Login"].Values["EmailId"];
                model.Password = Request.Cookies["Login"].Values["Password"];
                model.RememberMe = Convert.ToBoolean(Request.Cookies["Login"].Values["RememberMe"]);
            }
            return View(model);
            
        }

        [HttpPost]
        [AllowAnonymous]
        public ActionResult Login(UserProfileModel u)
       {
            
            if (u.EmailId!=null && u.Password!=null)
            {
                 UserProfileModel user = _objWebReportingDbContext.Database
                        .SqlQuery<UserProfileModel>(
                            "Select EmployeeId,FirstName,ProfilePicture from dbo.UserProfile where EmailId=@EmailId and Password=@Password and RoleId=1",
                            new SqlParameter("EmailId", u.EmailId),
                            new SqlParameter("Password", u.Password)).FirstOrDefault();


                if (user != null)
                {
                    FormsAuthentication.SetAuthCookie(u.EmailId, u.RememberMe);
                    if (u.RememberMe)
                    {
                        
                        cookie.Values.Add("Email", u.EmailId);
                        cookie.Values.Add("Password", u.Password);
                        cookie.Values.Add("Rememberme", (u.RememberMe).ToString());
                        cookie.Expires = DateTime.Now.AddDays(15);
                        Response.Cookies.Add(cookie);
                       
                    }
                    else
                    {
                        Response.Cookies["Login"].Values["Rememberme"] = "false";
                        //cookie.Values.Remove(Convert.ToBoolean(Request.Cookies["Login"].Values["Rememberme"]).ToString());
                    }

                        ApplicationSession.Login(user);
                        Session["Id"] = user.EmployeeId;
                        Session["username"] = user.FirstName;
                        Session["ProfilePicture"] = user.ProfilePicture;
                        Session["rememberme"] = u.RememberMe;
                        // LoggedInCkeck.g_IsLoggedIn = Session["IsLoggedIn"] == null ? false : (bool)Session["IsLoggedIn"];
                        return RedirectToAction("Dashboard", "UserProfile");
                }
                else
                {
                    ModelState.AddModelError("", "Invalid User Name or Password ");
                    TempData["message"] = "Your UserName Password does not match";
                    //return RedirectToAction("Login");
                }
            }
                
            
            return View(u);
        }
      
        public ActionResult DashBoard()
        {
            return View();
        }
        
        [HttpGet]
        [AllowAnonymous]
        public ActionResult Register()
        {
            return View();
        }

        [HttpPost]
        //[ValidateAntiForgeryToken]
        public ActionResult Register(RegisterModel model, HttpPostedFileBase file)
        {
            try
            {
                if (file != null)
                {
                    var allowedExtensions = new[] {
                                           ".Jpg", ".png", ".jpg", ".jpeg"
                                            };
                    var fileName = Path.GetFileName(file.FileName); //getting only file name(ex-ganesh.jpg)
                    var ext = Path.GetExtension(file.FileName); //getting the extension(ex-.jpg)
                    if (allowedExtensions.Contains(ext)) //check what type of extension
                    {
                        string name = Path.GetFileNameWithoutExtension(fileName); //getting file name without extension
                        string myfile = name + "_" + model.FirstName + ext; //appending the name with id  //store the file inside ~/project folder(Img)
                        var path = Path.Combine(Server.MapPath("~/UserProfilePic"), myfile);
                        model.ProfilePicture = _profileImagesPath + myfile; //model.CreatedDate = ApplicationSession.CurrentUser.EmployeeId;
                        file.SaveAs(path);
                    }
                    else
                    {
                        ModelState.AddModelError("", "Please select the image in .Jpg .png .jpg .jpeg Extensions Only!!");
                    }
                }
                else
                {
                    model.ProfilePicture = "";
                }

                int value = _objWebReportingDbContext.Database.ExecuteSqlCommand("RegisterAdmin @EmployeeId=@EmployeeId,@FirstName=@FirstName,@LastName=@LastName,@EmailId=@EmailId,@Password=@Password,@ProfilePicture=@ProfilePicture",
                                new SqlParameter("EmployeeId", model.EmployeeId),
                                new SqlParameter("FirstName", model.FirstName),
                                new SqlParameter("LastName", model.LastName),
                                new SqlParameter("EmailId", model.EmailId),
                                new SqlParameter("Password", model.Password),
                                new SqlParameter("ProfilePicture", model.ProfilePicture));

                if (value > 0)
                {
                    TempData["Message"] = "You have Successfully Registered !";
                    TempData.Keep();
                    return RedirectToAction("ViewUser");
                }

                //TempData["Message"] = "Something went wrong,Try Again!";
                //TempData.Keep();
                //return RedirectToAction("Register");
                //}
                //else
                //{
                //    TempData["Message"] = "This EmailId already exist !";
                //    TempData.Keep();
                //    return RedirectToAction("Register");
                //}
            }
            catch (Exception ex)
            {
                return Json(ex.Message);
            }
            return View(model);
        }
        
        [HttpGet]
        public ActionResult ForgetPassword()
        {
            
            return View();
        }

        [HttpPost]
        [AllowAnonymous]
        public async Task<ActionResult> ForgetPassword(ForgetPasswordModel model,string emailid)
        {
            if(ModelState.IsValid)
            {

           
            Guid EmployeeId = _objWebReportingDbContext.Database.SqlQuery<Guid>("Select EmployeeId from dbo.UserProfile where EmailId = @Email", new SqlParameter("@Email", emailid)).FirstOrDefault();

            if (EmployeeId != null)
            {
                var message = new MailMessage();
                message.To.Add(new MailAddress(emailid));  //replace with valid value
                message.From = new MailAddress("testifiedemail@gmail.com");  //replace with valid value
                message.Subject = "Click Here To Change Password!";
                    // message.Body = "Please Click on Link To Change Password<br/>" + ChangePasswordUrl + EmployeeId;
                    message.Body = "Please Click on Link To Change Password<br/> <a href = '" + ChangePasswordUrl + EmployeeId + "' > "+ ChangePasswordUrl + EmployeeId+" </a>";
               

                  message.IsBodyHtml = true;

                using (var smtp = new SmtpClient())
                {
                    var credential = new NetworkCredential
                    {
                        UserName = "Password@rfid-data.com",  //replace with valid value
                        Password = "20172018" //replace with valid value
                    };
                    smtp.Credentials = credential;
                    //smtp.Host = "smtp.gmail.com";
                     smtp.Host = "relay-hosting.secureserver.net";
                     smtp.Port = 465;
                    smtp.EnableSsl = true;
                    await smtp.SendMailAsync(message);

                }
                ViewBag.Message = "Link has been send successfully";
            }
            }
            else
            {
                ModelState.AddModelError("", "Invalid user name");
            }
            return View("ForgetPassword");
        }
        
        [HttpGet]
        public ActionResult ResetPassword(Guid? EmployeeId)
        {
            ViewBag.EmployeeId = EmployeeId;
            return View();
        }
        
        [HttpPost]
        public async Task<ActionResult> ResetPassword(ResetPasswordModel objResetPasswordModel, Guid EmployeeId)
        {
            try
            {
                ViewBag.EmployeeId = EmployeeId;
                if (ModelState.IsValid)
                {
                    int effectedRecord = _objWebReportingDbContext.Database.ExecuteSqlCommand("update dbo.UserProfile set Password=@Password where EmployeeId=@EmployeeId ", new SqlParameter("EmployeeId", EmployeeId), new SqlParameter("Password", objResetPasswordModel.Password));
                    if (effectedRecord > 0)
                    {

                        string EmailId = _objWebReportingDbContext.Database.SqlQuery<string>("Select EmailId from dbo.UserProfile Where EmployeeId=@EmployeeId" , new SqlParameter("EmployeeId", EmployeeId)).FirstOrDefault();

                        var message = new MailMessage();
                        message.To.Add(new MailAddress(EmailId));  //replace with valid value
                        message.From = new MailAddress("testifiedemail@gmail.com");  //replace with valid value
                        message.Subject = "Password Reset!";
                        message.Body = "Your New Password Is<br/>" + objResetPasswordModel.Password;
                        message.IsBodyHtml = true;

                        using (var smtp = new SmtpClient())
                        {
                            var credential = new NetworkCredential
                            {
                                UserName = "testifiedemail@gmail.com",  // replace with valid value
                                Password = "testifiedpassword@hackfree"  // replace with valid value
                            };
                            smtp.Credentials = credential;
                            smtp.Host = "smtp.gmail.com";
                            smtp.Port = 25;
                            smtp.EnableSsl = false;
                            await smtp.SendMailAsync(message);

                        }
                        return RedirectToAction("Login");
                    }
                    else
                    {
                        return View();
                    }
                }
                ViewBag.Message = "Password has been sent to your EmailId";
                return View();
            }
            catch(Exception ex)
            {
                return Json(ex.Message);
            }
            

        }

        public ActionResult Logout()
        {
            ApplicationSession.Logout();
            return RedirectToAction("Login");
            
        }
             
        public ActionResult ViewUser(string UserName,string Email,string EmpId)
        {
            try
            {
                ViewBag.ProfilePicture = Directory.EnumerateFiles(Server.MapPath("~/UserProfilePic"))
                             .Select(fn => "~/UserProfilePic/" + Path.GetFileName(fn));

                //RegisterModel model = new RegisterModel();
                List<RegisterModel> ListofObjects = null;

                if (UserName != null || Email != null || EmpId != null)
                {
                    List<RegisterModel> model = _objWebReportingDbContext.Database.SqlQuery<RegisterModel>("ListOfUser  @UserName=@UserName,@Email=@Email,@EmpId=@EmpId",
                                                 new SqlParameter("UserName", UserName),
                                                  new SqlParameter("Email", Email),
                                                  new SqlParameter("EmpId", EmpId)).ToList();
                    return View(model);
                }
                else
                {
                    ListofObjects = _objWebReportingDbContext.Database.SqlQuery<RegisterModel>("ListOfUser").ToList();
                    return View(ListofObjects);
                }
            }
            catch (Exception ex)
            {
                return Json(ex.Message);
            }
        }
        
        public ActionResult UserDetailsView(Guid id)
        {
            try
            {
                UserDetails model = _objWebReportingDbContext.Database.SqlQuery<UserDetails>("GetUserDetail @EmployeeId=@EmployeeId",
                                     new SqlParameter("EmployeeId", id)).FirstOrDefault();

              
                return View(model);
            }
            catch(Exception ex)
            {
                return Json(ex.Message);
            }
          
        } 

        [HttpGet]
        public ActionResult EditUserDetails(Guid id)
        {
            try
            {
                EditUser model = _objWebReportingDbContext.Database.SqlQuery<EditUser>("EditUser @EmployeeId=@EmployeeId",
                                    new SqlParameter("@EmployeeId", id)).FirstOrDefault();

                return View(model);
            }
           
            catch (Exception ex)
            {
                return Json(ex.Message);
            }
            
        }
        
        [HttpPost]
        public ActionResult EditUserDetails(EditUser model,Guid id)
        {
            try
            {
                int value = _objWebReportingDbContext.Database.ExecuteSqlCommand("UpdateUser @EmployeeId=@EmployeeId,@FirstName=@FirstName,@LastName=@LastName,@IsActive=@IsActive",
                                new SqlParameter("EmployeeId",id),
                                new SqlParameter("FirstName", model.FirstName),
                                new SqlParameter("LastName", model.LastName),
                                new SqlParameter("IsActive", model.status));

                if(value>0)
                {
                    TempData["message"] = "Your Details Edited successfully";
                    return RedirectToAction("ViewUser");
                }
                else
                {
                    TempData["message"] = "Please enter the details in specified format";
                    return RedirectToAction("ViewUser");
                }

            }
            catch(Exception ex)
            {
                return Json(ex.Message);
            }          
        }
        
        [HttpGet]
        public ActionResult DeleteUser(Guid id)
        {
            try
            {

                int value = _objWebReportingDbContext.Database.ExecuteSqlCommand("DeleteUser @EmployeeId=@EmployeeId",
                                    new SqlParameter("@EmployeeId", id));

                if (value > 0)
                {
                    return RedirectToAction("ViewUser");
                }
                else
                {
                    TempData["Message"] = "User Not Deleted Please Try again";
                }


            }
            catch (Exception ex)
            {
                return Json(ex.Message);
            }
            return View();
        }
    }
}
            
           
     

