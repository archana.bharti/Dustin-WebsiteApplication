﻿using DustinReporting.Data;
using ExcelDataReader;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.OleDb;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Xml;

namespace DustinReporting.Admin.Controllers
{
    public class ImortUserFilesController : Controller
    {
        private WebReportingDbContext _objWebReportingDbContext = new WebReportingDbContext();

        [HttpGet]
        public ActionResult UploadXls()
        {
            return View();
        }



        [HttpPost]
        public ActionResult UploadXls(HttpPostedFileBase file, int value)
        {
            try
            {
                DataSet ds = new DataSet(); 
                if (Request.Files["file"].ContentLength > 0)
                {
                    string fileExtension = System.IO.Path.GetExtension(Request.Files["file"].FileName);
                    if (fileExtension == ".xls" || fileExtension == ".xlsx")
                    {
                        var clientPath = System.IO.Path.GetDirectoryName(file.FileName);
                        string fileLocation = Server.MapPath("~/UploadFile/") + Request.Files["file"].FileName;
                        if (System.IO.File.Exists(fileLocation))
                        {
                            System.IO.File.Delete(fileLocation);
                        }
                        Request.Files["file"].SaveAs(fileLocation);
                        string excelConnectionString = string.Empty;
                        excelConnectionString = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + fileLocation + ";Extended Properties=\"Excel 12.0;HDR=Yes;IMEX=2\"";
                        //connection String for xls file format.
                        if (fileExtension == ".xls")
                        {
                            //excelConnectionString = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=180.151.232.92;initial catalog=DustinReporting;user id=sandbox;password=vinove@123;MultipleActiveResultSets=True;App=EntityFramework&quot;" + fileLocation + ";Extended Properties=\"Excel 8.0;HDR=Yes;IMEX=2\"";
                            excelConnectionString = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + fileLocation + ";Extended Properties=\"Excel 12.0;HDR=Yes;IMEX=2\"";
                        }
                        //connection String for xlsx file format.
                        else if (fileExtension == ".xlsx")
                        {
                            //"Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + path1 + ";Extended Properties=\"Excel 12.0;HDR=Yes;IMEX=2\"";
                            //excelConnectionString = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=180.151.232.92;initial catalog=DustinReporting;user id=sandbox;password=vinove@123;MultipleActiveResultSets=True;App=EntityFramework&quot;" + fileLocation + ";Extended Properties=\"Excel 12.0;HDR=Yes;IMEX=2\"";
                            excelConnectionString = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + fileLocation + ";Extended Properties=\"Excel 12.0;HDR=Yes;IMEX=2\"";
                        }
                        
                        //Create Connection to Excel work book and add oledb namespace
                        OleDbConnection excelConnection = new OleDbConnection(excelConnectionString);
                        excelConnection.Open();
                        DataTable dt = new DataTable();

                        dt = excelConnection.GetOleDbSchemaTable(OleDbSchemaGuid.Tables, null);

                        if (dt == null)
                        {
                            return null;
                        }

                        excelConnection.Close();

                        String[] excelSheets = new String[dt.Rows.Count];
                        int t = 0;

                        //Excel data saves in temp file here.
                        foreach (DataRow row in dt.Rows)
                        {
                            excelSheets[t] = row["TABLE_NAME"].ToString();
                            t++;
                        }
                        OleDbConnection excelConnection1 = new OleDbConnection(excelConnectionString);
                        string query = string.Format("Select * from [{0}]", excelSheets[0]);
                        using (OleDbDataAdapter dataAdapter = new OleDbDataAdapter(query, excelConnection1))
                        {
                            dataAdapter.Fill(ds);
                        }
                    }
                    if (value == 1)//for delete equipment
                    {
                        int eqip = _objWebReportingDbContext.Database.ExecuteSqlCommand("delete from EquipmentID");
                    }
                    if (value == 2)//for deleting apprelVest
                    {
                        int vest = _objWebReportingDbContext.Database.ExecuteSqlCommand("delete from  ApparelVests");
                    }
                    if (value == 3)//for deleting apprelHardHat
                    {
                        int hat = _objWebReportingDbContext.Database.ExecuteSqlCommand("delete from ApparelHardHats");
                    }


                    if (fileExtension.ToString().ToLower().Equals(".xml"))
                    {
                       
                        string fileLocation = Server.MapPath("~/Content/") + Request.Files["FileUpload"].FileName;
                        if (System.IO.File.Exists(fileLocation))
                        {
                            System.IO.File.Delete(fileLocation);
                        }

                        Request.Files["FileUpload"].SaveAs(fileLocation);
                        XmlTextReader xmlreader = new XmlTextReader(fileLocation);
                        // DataSet ds = new DataSet();
                        ds.ReadXml(xmlreader);
                        xmlreader.Close();
                    }
                   
                    for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                    {
                        WebReportingDbContext _objWebReportingDbContext = new WebReportingDbContext();
                        if (value == 1)//equip
                        {
                            if (ds.Tables[0].Rows[i][0] != null)//Sensornodeid
                            {

                                // string senId = _objWebReportingDbContext.Database.SqlQuery<string>("select SensorNodeID from EquipmentID where SensorNodeID ='" + ds.Tables[0].Rows[i][0].ToString().Trim() + "'").FirstOrDefault();

                                // if (string.IsNullOrEmpty(senId))
                                // {
                                int roweffected = _objWebReportingDbContext.Database.ExecuteSqlCommand("SaveEquipmentID @SensorNodeID=@SensorNodeID,@Company=@Company,@EquipmentType=@EquipmentType,@OperatorName=@OperatorName,@VehicleRegistration=@VehicleRegistration,@Make=@Make,@InCabDisplayUnitID=@InCabDisplayUnitID,@InstallationDate=@InstallationDate",
                                                                                                        new SqlParameter("SensorNodeID", ds.Tables[0].Rows[i][0].ToString().Trim()),
                                                                                                        new SqlParameter("Company", ds.Tables[0].Rows[i][1].ToString().Trim()),
                                                                                                        new SqlParameter("EquipmentType", ds.Tables[0].Rows[i][2].ToString().Trim()),
                                                                                                        new SqlParameter("OperatorName", ds.Tables[0].Rows[i][3].ToString().Trim()),
                                                                                                        new SqlParameter("VehicleRegistration", ds.Tables[0].Rows[i][4].ToString().Trim()),
                                                                                                        new SqlParameter("Make", ds.Tables[0].Rows[i][5].ToString().Trim()),
                                                                                                        new SqlParameter("InCabDisplayUnitID", ds.Tables[0].Rows[i][6].ToString().Trim()),
                                                                                                        new SqlParameter("InstallationDate", ds.Tables[0].Rows[i][7].ToString().Trim()));
                               // }
                            }
                        }
                        else if (value == 2)//vest
                        {
                            if (ds.Tables[0].Rows[i][6] != null)//UniqueIDVest
                            {
                                //    string appId = _objWebReportingDbContext.Database.SqlQuery<string>("select UniqueIDVest from ApparelVests where UniqueIDVest ='" + ds.Tables[0].Rows[i][6].ToString().Trim() + "'").FirstOrDefault();

                                //    if (string.IsNullOrEmpty(appId))
                                //    {
                                int roweffected = _objWebReportingDbContext.Database.ExecuteSqlCommand("SaveApparelVests @SystemID=@SystemID,@Company=@Company,@ProjectTeam=@ProjectTeam,@Surname=@Surname,@FirstNames=@FirstNames,@Position=@Position,@UniqueIDVest=@UniqueIDVest,@Supervisor=@Supervisor,@IssueDate=@IssueDate",
                                                                                                   new SqlParameter("SystemID", ds.Tables[0].Rows[i][0].ToString().Trim()),
                                                                                                   new SqlParameter("Company", ds.Tables[0].Rows[i][1].ToString().Trim()),
                                                                                                   new SqlParameter("ProjectTeam", ds.Tables[0].Rows[i][2].ToString().Trim()),
                                                                                                   new SqlParameter("Surname", ds.Tables[0].Rows[i][3].ToString().Trim()),
                                                                                                   new SqlParameter("FirstNames", ds.Tables[0].Rows[i][4].ToString().Trim()),
                                                                                                   new SqlParameter("Position", ds.Tables[0].Rows[i][5].ToString().Trim()),
                                                                                                   new SqlParameter("UniqueIDVest", ds.Tables[0].Rows[i][6].ToString().Trim()),
                                                                                                   new SqlParameter("Supervisor", ds.Tables[0].Rows[i][7].ToString().Trim()),
                                                                                                   new SqlParameter("IssueDate", ds.Tables[0].Rows[i][8].ToString().Trim()));
                               // }
                            }
                        }
                        else if (value == 3)//hat
                        {
                            if (ds.Tables[0].Rows[i][6] != null)//UniqueIDHardHat
                            {
                                //    string hatId = _objWebReportingDbContext.Database.SqlQuery<string>("select UniqueIDHardHat from ApparelHardHats where UniqueIDHardHat ='" + ds.Tables[0].Rows[i][6].ToString().Trim() + "'").FirstOrDefault();

                                //    if (string.IsNullOrEmpty(hatId))
                                //    {
                                int roweffected = _objWebReportingDbContext.Database.ExecuteSqlCommand("SaveApparelHardHats @SystemID=@SystemID,@Company=@Company,@Project=@Project,@Surname=@Surname,@FirstNames=@FirstNames,@Position=@Position,@UniqueIDHardHat=@UniqueIDHardHat,@Supervisor=@Supervisor,@Date=@Date,@IssueDate=@IssueDate",
                                                                                                  new SqlParameter("SystemID", ds.Tables[0].Rows[i][0].ToString().Trim()),
                                                                                                  new SqlParameter("Company", ds.Tables[0].Rows[i][1].ToString().Trim()),
                                                                                                  new SqlParameter("Project", ds.Tables[0].Rows[i][2].ToString().Trim()),
                                                                                                  new SqlParameter("Surname", ds.Tables[0].Rows[i][3].ToString().Trim()),
                                                                                                  new SqlParameter("FirstNames", ds.Tables[0].Rows[i][4].ToString().Trim()),
                                                                                                  new SqlParameter("Position", ds.Tables[0].Rows[i][5].ToString().Trim()),
                                                                                                  new SqlParameter("UniqueIDHardHat", ds.Tables[0].Rows[i][6].ToString().Trim()),
                                                                                                  new SqlParameter("Supervisor", ds.Tables[0].Rows[i][7].ToString().Trim()),
                                                                                                  new SqlParameter("Date", ds.Tables[0].Rows[i][8].ToString().Trim()),
                                                                                                  new SqlParameter("IssueDate", ds.Tables[0].Rows[i][9].ToString().Trim()));
                                //}
                            }
                        }
                       
                    }
                }

                ViewBag.Message = "File uploaded Successfully";
                return View();
            }
            catch (Exception ex)
            {
                StreamWriter sw = new StreamWriter(AppDomain.CurrentDomain.BaseDirectory + "\\error.txt", true);
                sw.WriteLine(DateTime.Now.ToString() + ": " + ex.Message);
                sw.Flush();
                sw.Close();
                ViewBag.Message = "Incorrect format";
                return View();
            }
        }

    }
}

