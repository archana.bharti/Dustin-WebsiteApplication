﻿using DustinReporting.Data.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DustinReporting.Admin.Models
{
    [Serializable()]
    public class ApplicationSession
    {
        private static string key = "Login";
      
        public static Guid EmployeeId { get; set; }
        public static string UserName { get; set; }
            public static void Login(UserProfileModel objUser)
            {

                System.Web.HttpContext.Current.Session[key] = objUser;
                System.Web.HttpContext.Current.Session.Timeout = 60;
                UserName = objUser.FirstName;
                EmployeeId = objUser.EmployeeId;
              }


            public static UserProfileModel CurrentUser
            {
                get
                {
                    if ((System.Web.HttpContext.Current.Session != null))
                    {
                        return (UserProfileModel)System.Web.HttpContext.Current.Session[key];
                    }
                    else
                    {
                        return null;
                    }
                }
            }
            public static void Logout()
            {
                System.Web.HttpContext.Current.Session.Clear();
                System.Web.HttpContext.Current.Session.Abandon();
            }
        }
}