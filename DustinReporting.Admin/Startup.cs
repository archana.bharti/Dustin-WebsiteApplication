﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(DustinReporting.Admin.Startup))]
namespace DustinReporting.Admin
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
