﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DustinReporting.Admin.Helper
{
    public static class SecondsConverter
    {
        public static double Convert(TimeSpan tm)
        {
            return tm.TotalSeconds;
        }
    }
}