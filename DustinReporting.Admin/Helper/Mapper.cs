﻿using DustinReporting.Data.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DustinReporting.Admin.Helper
{
    public static class Mapper
    {
        public static ScorecardBYORMetricModelForExcel MapByorMetricModel(ScorecardBYORMetricModel model)
        {
            ScorecardBYORMetricModelForExcel obj = new ScorecardBYORMetricModelForExcel()
            {

                ByorMetric = model.ByorMetric,
                Company = model.Company,
                FirstName = model.FirstName,
                LastName = model.LastName,
                ProjectTeam = model.ProjectTeam,
                SystemID = model.SystemID
            };
            return obj;
        }

        public static List<VMDetailEventReportExl> MapDetailEventReport(List<DetailedEventReport> model)
        {
            List<VMDetailEventReportExl> lst = new List<VMDetailEventReportExl>();

            foreach (DetailedEventReport item in model)
            {
                VMDetailEventReportExl obj = new VMDetailEventReportExl()
                {
                    SystemId = item.SystemId,
                    Surname = item.Surname,
                    FirstNames = item.FirstNames,
                    Datetime = item.Datetime,
                    Time = item.Time,
                    EventDuration = item.EventDuration,
                    Company = item.Company,
                    ApparelType = item.ApparelType,
                    ProjectTeam = item.ProjectTeam,
                    EquipmentType = item.EquipmentType,
                    OperatorName = item.OperatorName,
                    Position = item.Position,
                    Supervisor = item.Supervisor,
                    ApparelUniqueId = item.ApparelUniqueId
                    //public string Diff { get; set; }
                };
                lst.Add(obj);
            }
            return lst;
        }
    }
}