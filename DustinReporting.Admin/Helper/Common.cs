﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DustinReporting.Admin.Helper
{
    public class Common
    {

        public static IDictionary<string, object> searchConditions = new Dictionary<string, object>();

        public static object GetSearchConditionValue(IDictionary<string, object> searchConditions, string key)
        {
            object tempValue = null;

            if (searchConditions != null && searchConditions.Keys.Contains(key))
            {
                searchConditions.TryGetValue(key, out tempValue);
            }
            return tempValue;
        }
        public static bool SetSearchConditionValue(IDictionary<string, object> searchConditions, string key, object tempValue)
        {
            if (searchConditions != null && searchConditions.Keys.Contains(key))
            {
                searchConditions[key] = tempValue;
            }
            else
            {
                searchConditions.Add(new KeyValuePair<string, object>(key, tempValue));
            }
            return true;
        }
    }
}